package com.example.demo.dto;

import java.util.List;

import lombok.Data;
import org.springframework.http.HttpStatus;

import com.example.demo.exception.bookexception.BusinessErrorCode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse<T> {
	String message;
	List<String> details;
	BusinessErrorCode businessQueryErrorCode;
	HttpStatus httpStatus;

	public ErrorResponse(String message, List<String> details) {
		super();
		this.message = message;
		this.details = details;
	}
}

package com.example.demo.dto;

import com.example.demo.entity.Book;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {

//  @NotNull(message = "BookId should not be empty")
  @Id
  @GeneratedValue
  Integer id;

  @NotEmpty(message = "Code should not be empty")
  @Size(max = 20, message = "Size of code should not be greater than 20")
  String code;

  @NotEmpty(message = "Name should not be empty")
  @Size(max = 255, message = "Size of name should not be greater than 255")
  String name;

  @Size(max = 500, message = "Size of description should not be greate than 500")
  String description;

  @NotEmpty(message = "Category should not be empty")
  @Size(max = 255, message = "Size of category should not be greater than 255")
  String category;

  @NotEmpty(message = "Author should not be empty")
  @Size(max = 255, message = "Size of author should not be greater than 255")
  String author;

  @Size(max = 255, message = "Size of publisher should not be greater than 255")
  String publisher;

  @NotEmpty(message = "Create User should not be empty")
  @Size(max = 255, message = "Size of create user should not be greater than 100")
  String createUser;

  @CreationTimestamp
  ZonedDateTime createDate;

  @NotEmpty(message = "Update user should not be empty")
  @Size(max = 255, message = "Size of update user should not be greater than 100")
  String updateUser;

  @UpdateTimestamp
  ZonedDateTime updateDate;

  private Set<UserDTO> userDTOs;

  public BookDTO(Book book) {
    this.fromEntity(book);
  }

  private BookDTO fromEntity(Book book) {
    BeanUtils.copyProperties(book, this);
    this.userDTOs = book.getUsers().stream().map(user -> new UserDTO(user)).collect(Collectors.toSet());
    BeanUtils.copyProperties(book.getUsers(), this.userDTOs);
    return this;
  }
}

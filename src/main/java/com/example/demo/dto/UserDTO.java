package com.example.demo.dto;

import com.example.demo.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

  private int id;
  private String userName;
  private String passWord;
  private boolean isAuthor;
  private Set<BookDTO> books;

  public UserDTO(User user) {
    this.fromEntity(user);
    // TODO: further configuration.
  }

  public UserDTO fromEntity(User user){
    // LEVEl 1
    BeanUtils.copyProperties(user, this);

    // LEVEL 2
    this.books = new HashSet<>();
    BeanUtils.copyProperties(user.getBooks(), this.books);
    return this;
  }

  public static UserDTO fromEntity2(User user) {
    UserDTO dto = new UserDTO();
    BeanUtils.copyProperties(user, dto);
    // TODO: copy book
    return dto;
  }

  public User toEntity(){
    User user  = new User();
    return user;
  }
  @JsonIgnore
  public String getPassWord() {
    return passWord;
  }

  @JsonProperty
  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }
}

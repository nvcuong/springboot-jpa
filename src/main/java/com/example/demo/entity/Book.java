package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.time.ZonedDateTime;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "book", schema = "springjpa")
public class Book {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column(name = "book_id", nullable = false)
  Integer id;

  @Column(name = "code", nullable = false)
  String code;

  @Column(name = "name", nullable = false)
  String name;

  @Column(name = "description")
  @Size(max = 500, message = "Size of description should not be greate than 500")
  String description;

  @Column(name = "category", nullable = false)
  String category;

  @Column(name = "author", nullable = false)
  String author;

  @Column(name = "publisher")
  @Size(max = 255, message = "Size of publisher should not be greater than 255")
  String publisher;

  @Column(name = "create_user", nullable = false)
  String createUser;

  @Column(name = "create_date", nullable = false)
  @CreationTimestamp
  ZonedDateTime createDate;

  @Column(name = "update_user", nullable = false)
  String updateUser;

  @Column(name = "update_date")
  @UpdateTimestamp
  ZonedDateTime updateDate;

  @ManyToMany(mappedBy = "books")
  private Set<User> users;
}

package com.example.demo.queryside.service;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.queryside.repository.UserQueryRepository;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailServiceImpl implements UserDetailsService {

  @Autowired
  private UserQueryRepository userQueryRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userQueryRepository.findByUserName(username);
    if (user == null) {
      throw new UsernameNotFoundException("Invalid username or password.");
    }

    Set<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
    Set<Role> roles = user.getRoles();

    for (Role role : roles) {
      grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
    }

//    Set<GrantedAuthority> grantedAuthoritySet2 = roles
//        .stream()
//        .map(role -> new SimpleGrantedAuthority("ROLE_".concat(role.getName())))
//        .collect(Collectors.toSet());

    return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassWord(), grantedAuthoritySet);
  }

}

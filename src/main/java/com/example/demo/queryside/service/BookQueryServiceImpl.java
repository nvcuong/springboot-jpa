package com.example.demo.queryside.service;

import com.example.demo.dto.BookDTO;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Book;
import com.example.demo.exception.bookexception.BookNotFoundException;
import com.example.demo.queryside.repository.BookQueryRepository;
import com.example.demo.utility.ValidParam;

@Service
public class BookQueryServiceImpl implements BookQueryService {
	@Autowired
	private  BookQueryRepository bookQueryRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<BookDTO> getAllBook() {
		List<Book> outPut = bookQueryRepository.findAll();
		if (outPut.isEmpty()) {
			throw new BookNotFoundException("Don't have any book");
		}
		return outPut.stream().map(BookDTO::new).collect(Collectors.toList());
	}

	@Override
	public BookDTO findByUid(Integer bookId) {
//		ValidParam.validBookId(uid);
		Book outPut = bookQueryRepository.findById(bookId);
		if (outPut == null) {
			throw new BookNotFoundException("Book with id is " + bookId + " Not Found");
		}
		return modelMapper.map(outPut,BookDTO.class);
	}

	@Override
	public BookDTO findByCode(String code) {
		Book outPut = bookQueryRepository.findByCode(code);
		if (outPut == null) {
			throw new BookNotFoundException("Book with code is " + code + " Not Found");
		}
		return modelMapper.map(outPut,BookDTO.class);
	}

	@Override
	public List<BookDTO> findByName(String name) {
		List<Book> outPuts = bookQueryRepository.findByName(name);
		if (outPuts.isEmpty()) {
			throw new BookNotFoundException("Book with name is " + name + " Not Found");
		}
		return outPuts.stream().map(BookDTO::new).collect(Collectors.toList());
	}

	@Override
	public List<BookDTO> findByCategory(String category) {
		List<Book> outPuts = bookQueryRepository.findByCategory(category);
		if (outPuts.isEmpty()) {
			throw new BookNotFoundException("Book with category is " + category + " Not Found");
		}
		return outPuts.stream().map(BookDTO::new).collect(Collectors.toList());
	}

	@Override
	public List<BookDTO> findByAuthor(String author) {
		List<Book> outPuts = bookQueryRepository.findByAuthor(author);
		if (outPuts.isEmpty()) {
			throw new BookNotFoundException("Book with author is " + author + " Not Found");
		}
		return outPuts.stream()
				.map(BookDTO::new)
				.collect(Collectors.toList());
	}

}

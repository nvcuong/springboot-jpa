package com.example.demo.queryside.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.exception.bookexception.BookNotFoundException;
import com.example.demo.queryside.repository.UserQueryRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserQueryServiceImpl implements UserQueryService {

  @Autowired
  private UserQueryRepository userQueryRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public List<UserDTO> findUsers() {
    List<User> existedUsers = userQueryRepository.findAll();
    if (existedUsers.isEmpty()) {
      throw new BookNotFoundException("Don't have any book");
    }
//		List<UserDTO> listDTO = new ArrayList<>();
//		for (int i = 0; i < outPut.size(); i++) {
//			BeanUtils.copyProperties(listDTO.get(i), outPut.get(i));
//		}
//		return  listDTO;
    return existedUsers.stream().map(UserDTO::new).collect(Collectors.toList());
  }

  @Override
  public UserDTO findByUserId(int uid) {
//		ValidParam.validBookId(uid);
    User outPut = userQueryRepository.findById(uid);
    if (outPut == null) {
      throw new BookNotFoundException("Book with id is " + uid + " Not Found");
    }
    return modelMapper.map(outPut, UserDTO.class);
  }


  @Override
  public UserDTO findByUserName(String name) {
    User u = userQueryRepository.findByUserName(name);
    if (u == null) {
      throw new BookNotFoundException("User with name is " + name + " Not Found");
    }
    return modelMapper.map(u, UserDTO.class);
  }

//  @Override
//  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//    User user = userQueryRepository.findByUserName(username);
//    if (user == null) {
//      throw new UsernameNotFoundException("Invalid username or password.");
//    }
//    return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassWord(), getAuthority());
//  }
//
//  private List<SimpleGrantedAuthority> getAuthority() {
//    return Arrays.asList(new SimpleGrantedAuthority("ADMIN"));
//  }

//  @Override
//  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//    User user = userQueryRepository.findByUserName(username);
//    if (user == null) {
//      throw new UsernameNotFoundException("Invalid username or password.");
//    }
//    Set<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
//    Set<Role> roles = user.getRoles();
//
//    for (Role role : roles) {
//      grantedAuthoritySet.add(new SimpleGrantedAuthority(role.getName()));
//    }
//    return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassWord(), grantedAuthoritySet);
//  }

}

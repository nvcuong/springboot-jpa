package com.example.demo.queryside.service;

import com.example.demo.dto.BookDTO;
import java.util.List;
import com.example.demo.entity.Book;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

public interface BookQueryService {
  List<BookDTO> getAllBook();

  BookDTO findByUid(Integer bookId);

  BookDTO findByCode(String code);

  List<BookDTO> findByName(String name);

  List<BookDTO> findByCategory(String category);

  List<BookDTO> findByAuthor(String author);
}

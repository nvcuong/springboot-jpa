package com.example.demo.queryside.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import java.util.List;

public interface UserQueryService {

  List<UserDTO> findUsers();

  UserDTO findByUserId(int userId);

  UserDTO findByUserName(String userName);

}

package com.example.demo.queryside.repository;

import com.example.demo.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserQueryRepository extends JpaRepository<User, Integer> {

  User findById(int userId);

  User findByUserName(String userName);

  List<User> findAll();

  User findByEmail(String email);
}

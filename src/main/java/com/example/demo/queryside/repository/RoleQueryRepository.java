package com.example.demo.queryside.repository;

import com.example.demo.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleQueryRepository extends JpaRepository<Role, Integer> {
    Role findByName(String roleName);
}

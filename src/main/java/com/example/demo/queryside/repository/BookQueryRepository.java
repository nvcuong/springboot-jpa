package com.example.demo.queryside.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import com.example.demo.entity.Book;
import org.springframework.data.repository.Repository;

public interface BookQueryRepository extends Repository<Book,Integer>{

	Book findById(int bookId);

	Book findByCode(String code);

	List<Book> findByName(String bookName);

	List<Book> findByCategory(String category);

	List<Book> findByAuthor(String author);

	List<Book> findAll();

	@Query("SELECT b.id FROM Book b")
	List<Integer> getListBookId();

	@Query("SELECT b.code FROM Book b")
	List<String> getListCode();
}

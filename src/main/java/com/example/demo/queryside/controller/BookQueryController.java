package com.example.demo.queryside.controller;

import com.example.demo.dto.BookDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.Book;
import com.example.demo.queryside.service.BookQueryService;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/books")
public class BookQueryController {

  @Autowired
  BookQueryService bookQueryService;

  @GetMapping(value = "")
  public ResponseEntity<List<BookDTO>> getAllBooks() {
    List<BookDTO> books = bookQueryService.getAllBook();
    return new ResponseEntity<>(books, HttpStatus.OK);
  }

  @GetMapping(value = "/id/{uid}")
  public ResponseEntity<BookDTO> findBookById(@PathVariable("uid") Integer uid) {
    BookDTO book = bookQueryService.findByUid(uid);
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @GetMapping(value = "/code/{code}")
  public ResponseEntity<BookDTO> findBookByCode(@PathVariable("code") String code) {
    BookDTO book = bookQueryService.findByCode(code);
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @GetMapping(value = "/name/{name}")
  public ResponseEntity<List<BookDTO>> findBookByName(@PathVariable("name") String name) {
    List<BookDTO> books = bookQueryService.findByName(name);
    return new ResponseEntity<>(books, HttpStatus.OK);
  }

  @GetMapping(value = "/category/{category}")
  public ResponseEntity<List<BookDTO>> findBookByCategory(@PathVariable("category") String category) {
    List<BookDTO> books = bookQueryService.findByCategory(category);
    return new ResponseEntity<>(books, HttpStatus.OK);
  }

  @GetMapping(value = "/author/{author}")
  public ResponseEntity<List<BookDTO>> findBookByAuthor(@PathVariable("author") String author) {
    List<BookDTO> books = bookQueryService.findByAuthor(author);
    return new ResponseEntity<>(books, HttpStatus.OK);
  }
}

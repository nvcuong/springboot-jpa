package com.example.demo.queryside.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.queryside.service.UserQueryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserQueryController {

  @Autowired
  private UserQueryService userQueryService;

  @GetMapping
  public ResponseEntity<List<UserDTO>> getAllUsers() {
    List<UserDTO> users = userQueryService.findUsers();
    return new ResponseEntity<>(users, HttpStatus.OK);
  }

  @GetMapping(value = "/id/{id}")
  public ResponseEntity<UserDTO> findBookById(@PathVariable("id") Integer id) {
    UserDTO book = userQueryService.findByUserId(id);
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @GetMapping(value = "/name/{name}")
  public ResponseEntity<UserDTO> findUserByName(@PathVariable("name") String name) {
    UserDTO users = userQueryService.findByUserName(name);
    return new ResponseEntity<>(users, HttpStatus.OK);
  }

}

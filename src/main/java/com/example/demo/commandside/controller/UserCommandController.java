package com.example.demo.commandside.controller;

import com.example.demo.commandside.service.UserCommandService;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserCommandController {

  @Autowired
  private UserCommandService userCommandService;

  @PutMapping(value = "/update")
  public ResponseEntity<User> updateUser(@Valid @RequestBody UserDTO userDTO) {
    User outPut = userCommandService.updateUser(userDTO);
    return new ResponseEntity<>(outPut, HttpStatus.OK);
  }

  @DeleteMapping(value = "/delete/{id}")
  public ResponseEntity<User> deleteUser(@PathVariable("id") Integer id) {
    User outPut = userCommandService.deleteUser(id);
    return new ResponseEntity<>(outPut, HttpStatus.OK);
  }
}

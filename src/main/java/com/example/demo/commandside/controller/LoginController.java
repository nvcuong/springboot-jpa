package com.example.demo.commandside.controller;

import com.example.demo.commandside.service.UserCommandService;
import com.example.demo.dto.AuthToken;
import com.example.demo.dto.LoginDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.jwt.JwtService;
import com.example.demo.queryside.service.UserQueryService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping
public class LoginController {


  @Autowired
  private JwtService jwtService;
  @Autowired
  private UserQueryService userQueryService;

  @Autowired
  private UserCommandService userCommandService;

  @PostMapping(value = "/register")
  public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) {
    User outPut = userCommandService.createUser(userDTO);
    return new ResponseEntity<>(outPut, HttpStatus.CREATED);
  }

  @PostMapping(value = "/login")
  public ResponseEntity<AuthToken> login(@RequestBody LoginDTO loginDTO) throws AuthenticationException {
//    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getPassword(), loginDTO.getUsername()));
    final UserDTO userDTO = userQueryService.findByUserName(loginDTO.getUsername());
    final String token = jwtService.generateToken(userDTO);
    return new ResponseEntity<>(new AuthToken(token, userDTO.getUserName()), HttpStatus.OK);
  }

}

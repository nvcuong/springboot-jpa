package com.example.demo.commandside.controller;

import com.example.demo.commandside.service.BookCommandService;
import com.example.demo.dto.BookDTO;
import com.example.demo.entity.Book;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/books")
public class BookCommandController {

  @Autowired
  private BookCommandService bookCommandService;

  @PostMapping(value = "/create")
  public ResponseEntity<Book> createBook(@Valid @RequestBody BookDTO bookDTO) {
    Book outPut = bookCommandService.createBook(bookDTO);
    return new ResponseEntity<>(outPut, HttpStatus.CREATED);
  }

  @PutMapping(value = "/update")
  public ResponseEntity<Book> updateBook(@Valid @RequestBody BookDTO bookDTO) {
    Book outPut = bookCommandService.updateBook(bookDTO);
    return new ResponseEntity<>(outPut, HttpStatus.OK);
  }

  @DeleteMapping(value = "/delete/{uid}")
  public ResponseEntity<Book> deleteBook(@PathVariable("uid") Integer uid) {
    Book outPut = bookCommandService.deleteBook(uid);
    return new ResponseEntity<>(outPut, HttpStatus.OK);
  }
}

package com.example.demo.commandside.service;

import com.example.demo.dto.BookDTO;
import com.example.demo.entity.Book;
import org.springframework.stereotype.Service;

@Service
public interface BookCommandService {
  Book createBook(BookDTO bookDTO);
  Book updateBook(BookDTO bookDTO);
  Book deleteBook(int uid);
}

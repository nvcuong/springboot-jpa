package com.example.demo.commandside.service;

import com.example.demo.dto.BookDTO;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.commandside.repository.BookCommandRepository;
import com.example.demo.entity.Book;
import com.example.demo.exception.bookexception.BookNotFoundException;
import com.example.demo.queryside.repository.BookQueryRepository;
import com.example.demo.utility.ValidParam;

@Service
public class BookCommandServiceImpl implements BookCommandService {
	@Autowired
	BookCommandRepository bookCommandRepository;
	
	@Autowired
	BookQueryRepository bookQueryRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public Book createBook(BookDTO bookDTO) {
//		ValidParam.checkUniqueBookId(bookQueryRepository.getListBookId(), bookDTO.getId());
		ValidParam.checkUniqueCode(bookQueryRepository.getListCode(), bookDTO.getCode());
//		ValidParam.validBookId(bookDTO.getUid());
		return bookCommandRepository.save(modelMapper.map(bookDTO,Book.class));
	}

	@Override
	public Book updateBook(BookDTO bookDTO) {
		Book outPut = bookQueryRepository.findById(bookDTO.getId());
		if (outPut == null) {
			throw new BookNotFoundException("Book with id is " + bookDTO.getId() + " Not Found");
		}
		List<String> outPuts = bookQueryRepository.getListCode();
		outPuts.remove(bookDTO.getCode());
		ValidParam.checkUniqueCode(outPuts, bookDTO.getCode());
		return bookCommandRepository.save(modelMapper.map(bookDTO,Book.class));
	}

	@Override
	public Book deleteBook(int uid) {
		Book outPut = bookQueryRepository.findById(uid);
		if (outPut == null) {
			throw new BookNotFoundException("Book with id is " + uid + " Not Found");
		}
		bookCommandRepository.delete(outPut);
		return outPut;
	}
}

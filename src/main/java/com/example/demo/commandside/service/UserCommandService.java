package com.example.demo.commandside.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;

public interface UserCommandService {

  User createUser(UserDTO userDTO);

  User updateUser(UserDTO userDTO);

  User deleteUser(int id);
}

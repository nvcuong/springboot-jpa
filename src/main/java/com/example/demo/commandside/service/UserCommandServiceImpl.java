package com.example.demo.commandside.service;

import com.example.demo.commandside.repository.UserCommandRepository;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.exception.bookexception.BookNotFoundException;
import com.example.demo.queryside.repository.UserQueryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserCommandServiceImpl implements UserCommandService {

  @Autowired
  private UserCommandRepository userCommandRepository;

  @Autowired
  private UserQueryRepository userQueryRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private PasswordEncoder bcryptEncoder;

  @Override
  public User createUser(UserDTO userDTO) {
//		ValidParam.checkUniqueUserId(userQueryRepository.getListUid(), UserDTO.getUid());
//		ValidParam.checkUniqueCode(UserQueryRepository.getListCode(), UserDTO.getCode());
//		ValidParam.validUserId(UserDTO.getUid());
    userDTO.setPassWord(bcryptEncoder.encode(userDTO.getPassWord()));
    return userCommandRepository.save(modelMapper.map(userDTO, User.class));
  }

  @Override
  public User updateUser(UserDTO userDTO) {
    User outPut = userQueryRepository.findById(userDTO.getId());
    if (outPut == null) {
      throw new BookNotFoundException("User with id is " + userDTO.getId() + " Not Found");
    }
    return userCommandRepository.save(modelMapper.map(outPut, User.class));
  }

  @Override
  public User deleteUser(int id) {
    User outPut = userQueryRepository.findById(id);
    if (outPut == null) {
      throw new BookNotFoundException("User with id is " + id + " Not Found");
    }
    userCommandRepository.delete(outPut);
    return outPut;
  }
}

package com.example.demo.commandside.repository;

import com.example.demo.entity.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCommandRepository extends CrudRepository<Book, Integer> {

}

package com.example.demo.jwt;

import static com.example.demo.constant.Constants.ACCESS_TOKEN_VALIDITY_SECONDS;
import static com.example.demo.constant.Constants.SIGNING_KEY;

import com.example.demo.dto.UserDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtService {

  @Value("${jwt.secret}")
  private String secret;

  private String doGenerateToken(String subject) {
    Claims claims = Jwts.claims().setSubject(subject);
//    claims.put("scopes", Arrays.asList(new SimpleGrantedAuthority("ADMIN")));
//    claims.put("scopes", Arrays.asList(new SimpleGrantedAuthority("ADMIN")));
    return Jwts.builder()
        .setClaims(claims)
        .setExpiration(new Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS * 1000))
        .signWith(SignatureAlgorithm.HS512, SIGNING_KEY)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .compact();
  }


  public String generateToken(UserDTO userDTO) {
    return doGenerateToken(userDTO.getUserName());
  }

  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(SIGNING_KEY).parseClaimsJws(token).getBody();
  }

  <T> T getClaimsFromToken(String token, Function<Claims, T> claimsTFunction) {
    final Claims claims = getAllClaimsFromToken(token);
    return claimsTFunction.apply(claims);
  }

  //get username by token
  String getUserNameByToken(String token) {
    return getClaimsFromToken(token, Claims::getSubject);
  }

  Date getTimeTokenLifeFromToken(String token) {
    return getClaimsFromToken(token, Claims::getExpiration);
  }

  private Boolean checkTimeToken(String token) {
    final Date timeTokenLife = getTimeTokenLifeFromToken(token);
    return timeTokenLife.before(new Date());
  }

  public Boolean checkToken(String token, UserDetails userDetails) {
    final String username = getUserNameByToken(token);
    return username.equals(userDetails.getUsername()) && !checkTimeToken(token);
  }

}
